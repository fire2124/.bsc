import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class InConsole extends Thread {

    private static int counter = 0;

    public static void main(String[] args) {

        new InConsole().start();
    }

    private static void getMoney() {
        ArrayList<String> currencynew=new  ArrayList<>() ;
        ArrayList<Integer> amount = new ArrayList<>();
        BufferedReader br;
        String line;
        int x=0,y=1;


        try {
            br = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Insert Money");
            System.out.println("First put currency, then amount");

            while (!(line = br.readLine()).equals("")) {

                if (line.equals("quit")) return;

                String[] input = line.split(" ");

                if ((input[0].length() != 3) &&  (Integer.parseInt(input[1])==0) ) {
                    System.out.println("You put bad cuurency and bad amount");
                    continue;
                }
                if ( input[0].length() == 3 )  currencynew.add(input[0]);
                else {
                    System.out.println("You put bad cuurency");
                    continue;
                }

                if(Integer.parseInt(input[1])!=0) amount.add(Integer.parseInt(input[1]));
                else{
                    System.out.println("You entered bad amount");
                    continue;
                }
                x=x+2;
                y=y+2;
            }

            TimerTask timerTask = new TimerTask() {

                @Override
                public void run() {
                    System.out.println("----------");
                    System.out.println("You put this currency and amount");
                    money();
                    System.out.println("TimerTask executing counter is: " + counter);
                    counter++;
                }

                private void money() {
                    ArrayList<Integer> help1 = new ArrayList<>();
                    ArrayList<Integer> help2 = new ArrayList<>();
                    ArrayList<Integer> help3 = new ArrayList<>();
                    ArrayList<Integer> help4 = new ArrayList<>();
                    ArrayList<Integer> help5 = new ArrayList<>();
                    ArrayList<Integer> help6 = new ArrayList<>();

                    double plus1=0;
                    double plus2=0;
                    double plus3=0;
                    double plus4=0;
                    double plus5=0;
                    double plus6=0;

                    String sth="";
                    //find index of equal currency
                    for (int i=0;i<currencynew.size();i++) {

                        String element = currencynew.get(i);


                        switch (element.toUpperCase()) {
                            case "USD":
                                help1.add(i);
                                break;
                            case "EUR":
                                help2.add(i);
                                break;
                            case "HKD":
                                help3.add(i);
                                break;
                            case "RMB":
                                help4.add(i);
                                break;
                            case "NZD":
                                help5.add(i);
                                break;
                            case "GBP":
                                help6.add(i);
                                break;
                        }
                    }

                    //plus
                    for (int i=0;i<help1.size();i++) {
                        //USD
                        plus1 = plus1 + amount.get(help1.get(i)) ;
                    }
                    for (int i=0;i<help2.size();i++) {
                        //EUR
                        plus2 = plus2 + amount.get(help2.get(i)) ;
                    }
                    for (int i=0;i<help3.size();i++) {
                        //HKD
                        plus3 = plus3 + amount.get(help3.get(i)) ;
                    }
                    for (int i=0;i<help4.size();i++) {
                        //RMB
                        plus4 = plus4 + amount.get(help4.get(i)) ;
                    }
                    for (int i=0;i<help5.size();i++) {
                        //NZD
                        plus5 = plus5 + amount.get(help5.get(i)) ;
                    }
                    for (int i=0;i<help6.size();i++) {
                        //GBP
                        plus6 = plus6 + amount.get(help6.get(i)) ;
                    }

                    //output
                    if(plus1!=0){
                        System.out.println( "USD "  + plus1 );
                    }

                    if(plus2!=0)
                    {
                        Double newAmount = (plus2*1.114675);
                        System.out.println( "EUR " + plus2 + " (" +(newAmount + " USD") + ")");
                    }

                    if(plus3!=0){
                        Double newAmount = (plus3*0.145308);
                        System.out.println( "HKD " + plus3 + " (" +(newAmount + " USD")+ ")");
                    }

                    if(plus4!=0)
                    {
                        Double newAmount = (plus4*0.6608);
                        System.out.println( "RMB " + plus4 + " (" +(newAmount + " USD")+ ")");
                    }

                    if(plus5!=0){
                        Double newAmount = (plus5*1.216985);
                        System.out.println( "NZD " + plus5 + " (" +(newAmount + " USD")+ ")");
                    }

                    if(plus6!=0){
                        Double newAmount = (plus6*1.216985);
                        System.out.println( "GBP " + plus6 + " (" +(newAmount + " USD")+ ")");
                    }
                }
            };

            Timer timer = new Timer("MyTimer");//create a new Timer
            timer.scheduleAtFixedRate(timerTask, 0, 60000);//this line starts the timer at the same time its executed


            br.close();
        }catch (NumberFormatException ignored){
            System.out.println("Bad position of currency or amount. Please put it in the right order.");
            getMoney();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        getMoney();
    }
}
