# **Payment Tracker**
    The program consists of two java classes and they are recording of payments to memory. 
    Fist class named Automat is taking amounts and currency from the text.txt . 
    Second named InConsole is taking amounts and currency from console.
    Once per minute they will print holl memory to the console. 

##**Getting Started**
    These instructions will get you a copy of the project up and running on your 
    local machine for development and testing purposes.

##**Prerequisites**
    You need to install Java 1.8.0_152 or higher.

##**Running and testing**

    1. Automat 
    
    Run Automat class. Put the name of the file .txt from resources, 
    in this case we will put file named text .
    If you put quit the program will shut down.
    After that the file will print the holl file to the console once per minute.
    If the file doesnt exits it will print that the file doesnt exist.
    
    2. InConsole
    
    Run InCosole. You are asked to put First Currency and then amount.
    If you put they in the wrong order it will asked yout to put it aqain in right order.
    
    If you put quit the program will shut down.
    
    If the currency has not the lenght 3 and the amount equals 0 it will print 
    that You put bad cuurency and bad amount
    
    If the currency has not the lenght 3 the program will print that You put bad cuurency.
    
    If the amount equals 0, the program will print that you put bad amount.
    
    < --- Notes --- > :
    Both programs will print the exchange rate compared to USD 
    
    Exchange rates:
    EUR -> USD : 1.114675
    HKD -> USD : 0.145308
    RMB -> USD : 0.6608
    NZD -> USD : 1.216985
    GBP -> USD : 1.216985
  
##**Built With**
    Java 1.8.0_152

##**Author**
    Filip Reichl

